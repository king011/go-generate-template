#include "cmd.h"
#include <boost/lexical_cast.hpp>

void subcommand_calculator(CLI::App &app)
{
    CLI::App *cmd = app.add_subcommand("calculator", "calculator for number");

    cmd->callback([&cmd = *cmd] {
        // 獲取 額外 參數
        std::vector<std::string> items = cmd.remaining();
        std::int64_t num = 0;
        for (const std::string &str : items)
        {
            try
            {
                num += boost::lexical_cast<std::int64_t>(str);
            }
            catch (const boost::bad_lexical_cast &e)
            {
                throw CLI::InvalidError(str);
            }
        }
        const CLI::Option &sub = *cmd.get_option("--sub");
        if (sub)
        {
            std::cout << -num << std::endl;
        }
        else
        {
            std::cout << num << std::endl;
        }
        // 通知 CLI11_PARSE 成功
        throw CLI::Success();
    });

    cmd->allow_extras();
    // 設置 參數
    cmd->add_flag("-s,--sub", "sub");
}
void subcommand_listen(CLI::App &app)
{
    CLI::App *cmd = app.add_subcommand("listen", "tcp/udp server listen");

    cmd->callback([&cmd = *cmd] {
        const CLI::Option &udp = *cmd.get_option("--udp");
        const std::string addr = cmd.get_option("--addr")->as<std::string>();
        const std::uint32_t port = cmd.get_option("--port")->as<std::uint32_t>();
        if (udp)
        {
            std::cout << "udp work at " << addr << ":" << port << std::endl;
        }
        else
        {
            std::cout << "tcp work at " << addr << ":" << port << std::endl;
        }
        // 通知 CLI11_PARSE 成功
        throw CLI::Success();
    });
    // 設置 參數
    cmd->add_flag("--udp", "work as udp");
    std::string addr = "127.0.0.1";
    cmd->add_option("-a,--addr",
                    addr, // 默認 值
                    "listen addr",
                    true // true 使用 默認值
    );
    std::uint32_t port = 1911;
    cmd->add_option("-p,--port",
                    port,
                    "listen port",
                    true);
}
