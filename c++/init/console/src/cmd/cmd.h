#pragma once

#include "../CLI11.hpp"

void subcommand_test(CLI::App &app, char *path);
void subcommand_calculator(CLI::App &app);
void subcommand_listen(CLI::App &app);