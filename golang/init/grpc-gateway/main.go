package main

import (
	"log"
	// _ "{{.Package}}/assets/en-US/statik"
	// _ "{{.Package}}/assets/static/statik"
	// _ "{{.Package}}/assets/zh-Hans/statik"
	// _ "{{.Package}}/assets/zh-Hant/statik"
	"{{.Package}}/cmd"
	_ "{{.Package}}/module/features/logger"
	_ "{{.Package}}/module/session"
	_ "{{.Package}}/module/system/debug"

	_ "github.com/golang/protobuf/proto"
)

func main() {
	log.SetFlags(log.LstdFlags | log.Lshortfile)
	if e := cmd.Execute(); e != nil {
		log.Fatalln(e)
	}
}
