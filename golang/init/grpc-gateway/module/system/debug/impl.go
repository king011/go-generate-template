package session

import (
	"context"
	"fmt"
	"runtime"
	"time"

	"{{.Package}}/logger"
	"{{.Package}}/management"
	"{{.Package}}/module"
	grpc_debug "{{.Package}}/protocol/system/debug"
	"{{.Package}}/utils"
	"{{.Package}}/version"

	"go.uber.org/zap"
	"google.golang.org/grpc/codes"
)

type _Impl struct {
	module.Service
}

func (s _Impl) Info(ctx context.Context, request *grpc_debug.InfoRequest) (response *grpc_debug.InfoResponse, e error) {
	// TAG := "system/debug Impl.Info"
	
	response = &grpc_debug.InfoResponse{
		Platform: fmt.Sprintf("%v %v %v", runtime.GOOS, runtime.GOARCH, runtime.Version()),
		Tag:      version.Tag,
		Commit:   version.Commit,
		Date:     version.Date,
	}
	return
}

func (s _Impl) Source(ctx context.Context, request *grpc_debug.SourceRequest) (response *grpc_debug.SourceResponse, e error) {
	// TAG := "system/debug Impl.Source"

	response = &grpc_debug.SourceResponse{
		GoMAXPROCS:   int32(runtime.GOMAXPROCS(0)),
		NumCPU:       int32(runtime.NumCPU()),
		NumCgoCall:   runtime.NumCgoCall(),
		NumGoroutine: int32(runtime.NumGoroutine()),
	}
	return
}

// Timer timer stream test
func (s _Impl) Timer(request *grpc_debug.TimerRequest, stream grpc_debug.Service_TimerServer) (e error) {
	TAG := "system/debug Impl.Timer"

	if request.Millisecond < 1 {
		e = s.Error(codes.InvalidArgument, `millisecond must larger than 0`)
		return
	}
	done := stream.Context().Done()
	work := true
	duration := time.Duration(request.Millisecond) * time.Millisecond
	t := time.NewTicker(duration)
	for work {
		select {
		case <-done:
			work = false
		case <-t.C:
			unix := time.Now().Unix()
			e = stream.Send(&grpc_debug.TimerResponse{
				Unix: unix,
			})
			if e == nil {
				if ce := logger.Logger.Check(zap.InfoLevel, TAG); ce != nil {
					ce.Write(
						zap.Error(e),
						zap.String(`duration`, duration.String()),
						zap.Int64(`unix`, unix),
					)
				}
			} else {
				work = false
				if ce := logger.Logger.Check(zap.WarnLevel, TAG); ce != nil {
					ce.Write(
						zap.Error(e),
						zap.String(`duration`, duration.String()),
						zap.Int64(`unix`, unix),
					)
				}
			}
		}
	}
	t.Stop()
	return
}

func (s _Impl) Reload(ctx context.Context, request *grpc_debug.ReloadRequest) (response *grpc_debug.ReloadResponse, e error) {
	TAG := "system/debug Impl.Reload"

	session, e := s.Session(ctx)
	if e != nil {
		if ce := logger.Logger.Check(zap.WarnLevel, TAG); ce != nil {
			ce.Write(
				zap.Error(e),
				zap.String("user", session.Name),
				zap.String("module", request.Module),
				zap.String("tag", request.Tag),
			)
		}
		return
	}
	e = management.Single().Reload(request.Module, request.Tag)
	if e != nil {
		if ce := logger.Logger.Check(zap.WarnLevel, TAG); ce != nil {
			ce.Write(
				zap.Error(e),
				zap.String("user", session.Name),
				zap.String("module", request.Module),
				zap.String("tag", request.Tag),
			)
		}
		return
	}

	if ce := logger.Logger.Check(zap.InfoLevel, TAG); ce != nil {
		ce.Write(
			zap.String("user", session.Name),
			zap.String("module", request.Module),
			zap.String("tag", request.Tag),
		)
	}
	response = &grpc_debug.ReloadResponse{}
	return
}

func (s _Impl) ClearCache(ctx context.Context, request *grpc_debug.ClearCacheRequest) (response *grpc_debug.ClearCacheResponse, e error) {
	TAG := "system/debug Impl.ClearCache"

	session, e := s.Session(ctx)
	if e != nil {
		if ce := logger.Logger.Check(zap.WarnLevel, TAG); ce != nil {
			ce.Write(
				zap.Error(e),
				zap.String("user", session.Name),
				zap.String("module", request.Module),
				zap.String("tag", request.Tag),
			)
		}
		return
	}
	e = management.Single().ClearCache(request.Module, request.Tag)
	if e != nil {
		if ce := logger.Logger.Check(zap.WarnLevel, TAG); ce != nil {
			ce.Write(
				zap.Error(e),
				zap.String("user", session.Name),
				zap.String("module", request.Module),
				zap.String("tag", request.Tag),
			)
		}
		return
	}

	if ce := logger.Logger.Check(zap.InfoLevel, TAG); ce != nil {
		ce.Write(
			zap.String("user", session.Name),
			zap.String("module", request.Module),
			zap.String("tag", request.Tag),
		)
	}
	response = &grpc_debug.ClearCacheResponse{}
	return
}

func (s _Impl) ClearDBCache(ctx context.Context, request *grpc_debug.ClearDBCacheRequest) (response *grpc_debug.ClearDBCacheResponse, e error) {
	TAG := "system/debug Impl.ClearDBCache"

	session, e := s.Session(ctx)
	if e != nil {
		if ce := logger.Logger.Check(zap.WarnLevel, TAG); ce != nil {
			ce.Write(
				zap.Error(e),
				zap.String("user", session.Name),
				zap.String("module", request.Module),
				zap.String("tag", request.Tag),
			)
		}
		return
	}
	e = management.Single().ClearDBCache(request.Module, request.Tag)
	if e != nil {
		if ce := logger.Logger.Check(zap.WarnLevel, TAG); ce != nil {
			ce.Write(
				zap.Error(e),
				zap.String("user", session.Name),
				zap.String("module", request.Module),
				zap.String("tag", request.Tag),
			)
		}
		return
	}

	if ce := logger.Logger.Check(zap.InfoLevel, TAG); ce != nil {
		ce.Write(
			zap.String("user", session.Name),
			zap.String("module", request.Module),
			zap.String("tag", request.Tag),
		)
	}
	response = &grpc_debug.ClearDBCacheResponse{}
	return
}

func (s _Impl) StartTime(ctx context.Context, request *grpc_debug.StartTimeRequest) (response *grpc_debug.StartTimeResponse, e error) {
	response = &grpc_debug.StartTimeResponse{
		Unix: _Start.Unix(),
	}
	return
}

func (s _Impl) Quit(ctx context.Context, request *grpc_debug.QuitRequest) (response *grpc_debug.QuitResponse, e error) {
	TAG := "system/debug Impl.Quit"

	session, e := s.Session(ctx)
	if e != nil {
		if ce := logger.Logger.Check(zap.WarnLevel, TAG); ce != nil {
			ce.Write(
				zap.Error(e),
			)
		}
		return
	}
	if ce := logger.Logger.Check(zap.InfoLevel, TAG); ce != nil {
		ce.Write(
			zap.String("user", session.Name),
		)
	}
	time.AfterFunc(time.Second, utils.Quit)
	response = &grpc_debug.QuitResponse{}
	return
}
