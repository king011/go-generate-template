package module

import (
	"context"
	"{{.Package}}/cookie"

	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

var (
	errSessionMiss    = status.Error(codes.PermissionDenied, `session miss`)
	errSessionTourist = status.Error(codes.PermissionDenied, `session tourist`)
)

// Service base service
type Service struct {
}

// Session 驗證 session 存在並返回
func (s Service) Session(ctx context.Context) (session *cookie.Session, e error) {
	session, e = cookie.FromContext(ctx)
	if e != nil {
		e = status.Error(codes.Unauthenticated, e.Error())
		return
	}
	if session == nil {
		e = errSessionMiss
	} else if session.Tourist {
		session = nil
		e = errSessionTourist
	}
	return
}

// Error new grpc error
func (s Service) Error(code codes.Code, msg string) error {
	return status.Error(code, msg)
}