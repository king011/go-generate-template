package session

import (
	"crypto/sha512"
	"encoding/hex"
	"{{.Package}}/cookie"
	"{{.Package}}/logger"
	"sync"

	"go.uber.org/zap"
)

var _db _DB

type _User struct {
	Name          string
	Password      string
	Authorization []int64
}
type _DB struct {
	sync.RWMutex
	keys map[string]*_User
}

func (d *_DB) set(items []_User) {
	d.Lock()
	d.keys = make(map[string]*_User)
	for _, item := range items {
		if item.Name == "" || item.Password == "" {
			continue
		}

		b := sha512.Sum512([]byte(item.Password))
		item.Password = hex.EncodeToString(b[:])
		if _, ok := d.keys[item.Name]; ok {
			if ce := logger.Logger.Check(zap.WarnLevel, `Set user already exists`); ce != nil {
				ce.Write(
					zap.String("name", item.Name),
				)
			}
			continue
		}
		d.keys[item.Name] = &item
	}
	d.Unlock()
}

// Manipulator .
type Manipulator struct {
}

// Login .
func (Manipulator) Login(name, password string) (session *cookie.Session, e error) {
	_db.RLock()
	defer _db.RUnlock()
	user := _db.keys[name]
	if user == nil || user.Password != password {
		return
	}
	session = &cookie.Session{
		Name:          user.Name,
		Authorization: user.Authorization,
	}
	return
}
