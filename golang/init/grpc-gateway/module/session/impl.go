package session

import (
	"context"
	"{{.Package}}/cookie"
	grpc_session "{{.Package}}/protocol/session"
	"net/http"
	"strconv"
	"time"

	"{{.Package}}/logger"
	"{{.Package}}/module"

	"go.uber.org/zap"
	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/metadata"
)

type _Impl struct {
	module.Service
}

func (s _Impl) Login(ctx context.Context, request *grpc_session.LoginRequest) (response *grpc_session.LoginResponse, e error) {
	TAG := "session Impl.Login"

	if request.Name == "" {
		e = s.Error(codes.InvalidArgument, "name not support nil")
		return
	}
	if request.Password == "" {
		e = s.Error(codes.InvalidArgument, "password not support nil")
		return
	}

	var m Manipulator
	session, e := m.Login(request.Name, request.Password)
	if e != nil {
		if ce := logger.Logger.Check(zap.WarnLevel, TAG); ce != nil {
			ce.Write(
				zap.Error(e),
			)
		}
		return
	}else if session == nil{
		e = s.Error(codes.NotFound, "name or password not correct")
		return
	}

	at := time.Now()
	value, e := session.Cookie()
	if e != nil {
		if ce := logger.Logger.Check(zap.WarnLevel, TAG); ce != nil {
			ce.Write(
				zap.Error(e),
			)
		}
		return
	}
	if ce := logger.Logger.Check(zap.InfoLevel, TAG); ce != nil {
		ce.Write(
			zap.String("name", session.Who()),
		)
	}
	maxage := cookie.MaxAge()
	response = &grpc_session.LoginResponse{
		Session: &grpc_session.Session{
			Name:          session.Name,
			Authorization: session.Authorization,
		},
		Value:  value,
		Maxage: maxage,
	}
	header := metadata.Pairs(
		`Cache-Control`, "max-age="+strconv.Itoa((int(maxage))),
		`Last-Modified`, at.UTC().Format(http.TimeFormat),
	)
	grpc.SetHeader(ctx, header)
	return
}

var emptyRestoreResponse grpc_session.RestoreResponse

func (s _Impl) Restore(ctx context.Context, request *grpc_session.RestoreRequest) (response *grpc_session.RestoreResponse, e error) {
	TAG := "session Impl.Restore"
	session, e := s.Session(ctx)
	if e != nil {
		if ce := logger.Logger.Check(zap.WarnLevel, TAG); ce != nil {
			ce.Write(
				zap.Error(e),
			)
		}
		return
	}

	var header []string
	if request.Maxage > 0 && request.At > 0 {
		now := time.Now()
		at := time.Unix(request.At, 0).Local()
		expired := at.Add(time.Duration(request.Maxage) * time.Second)
		maxage := expired.Sub(now) / time.Second
		if maxage < 0 {
			maxage = 0
		}
		header = append(header,
			`Cache-Control`, `max-age=`+strconv.Itoa((int(maxage))),
			`Last-Modified`, at.UTC().Format(http.TimeFormat),
		)
	} else {
		header = append(header,
			`Cache-Control`, `max-age=0`,
		)
	}

	if md, ok := metadata.FromIncomingContext(ctx); ok {
		strs := md.Get("If-Modified-Since")
		if strs != nil && len(strs) != 0 {
			header = append(header,
				`x-http-code`, strconv.Itoa(http.StatusNotModified),
			)
			grpc.SetHeader(ctx, metadata.Pairs(header...))
			response = &emptyRestoreResponse
			return
		}
	}

	if ce := logger.Logger.Check(zap.InfoLevel, TAG); ce != nil {
		ce.Write(
			zap.String("name", session.Who()),
		)
	}
	if header != nil {
		grpc.SetHeader(ctx, metadata.Pairs(header...))
	}
	response = &grpc_session.RestoreResponse{
		Session: &grpc_session.Session{
			Name:          session.Name,
			Authorization: session.Authorization,
		},
	}
	return
}
