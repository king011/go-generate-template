package v1

import (
	"{{.Package}}/web"

	"github.com/gin-gonic/gin"
)

// BaseURL .
const BaseURL = `v1`

// Helper 一些其它的 api
type Helper struct {
	web.Helper
}

// Register impl IController
func (h Helper) Register(router *gin.RouterGroup) {
	r := router.Group(BaseURL)

	ms := []web.IHelper{
		Other{},
		Logger{},
	}
	for _, m := range ms {
		m.Register(r)
	}
}
