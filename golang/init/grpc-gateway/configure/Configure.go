package configure

import (
	"encoding/json"
	"io/ioutil"
	"path/filepath"
	"sync"

	"github.com/google/go-jsonnet"
	"{{.Package}}/logger"
)

// Configure global configure
type Configure struct {
	Logger logger.Options
	HTTP   HTTP
	Cookie Cookie
	Module Module

	basePath string
	filename string
}

// Format format global configure
func (c *Configure) Format() (e error) {
	if e = c.HTTP.Format(c.basePath); e != nil {
		return
	}
	if e = c.Cookie.Format(c.basePath); e != nil {
		return
	}
	return
}
func (c *Configure) String() string {
	if c == nil {
		return "nil"
	}
	b, e := json.MarshalIndent(c, "", "	")
	if e != nil {
		return e.Error()
	}
	return string(b)
}

var _Configure Configure
var _RW sync.RWMutex

// Single single Configure
func Single() *Configure {
	return &_Configure
}

// Reload reload data
func Reload(id string) (cnf *Configure, e error) {
	var c struct {
		Module struct {
			Data map[string]json.RawMessage
		}
	}

	b, e := ioutil.ReadFile(_Configure.filename)
	if e != nil {
		return
	}
	vm := jsonnet.MakeVM()
	vm.Importer(&jsonnet.FileImporter{})
	var jsonStr string
	jsonStr, e = vm.EvaluateSnippet(_Configure.filename, string(b))
	if e != nil {
		return
	}
	b = []byte(jsonStr)
	e = json.Unmarshal(b, &c)
	if e != nil {
		return
	}
	_RW.Lock()
	if id == "" {
		_Configure.Module.Data = c.Module.Data
	} else {
		if c.Module.Data == nil {
			if _Configure.Module.Data != nil {
				delete(_Configure.Module.Data, id)
			}
		} else {
			if _Configure.Module.Data == nil {
				_Configure.Module.Data = make(map[string]json.RawMessage)
			}
			_Configure.Module.Data[id] = c.Module.Data[id]
		}
	}
	_RW.Unlock()
	cnf = &_Configure
	return
}

// BasePath .
func (c *Configure) BasePath() string {
	return c.basePath
}

// GetModuleData .
func (c *Configure) GetModuleData(id string) json.RawMessage {
	_RW.Lock()
	defer _RW.Unlock()
	if c.Module.Data == nil {
		return nil
	}
	return c.Module.Data[id]
}

// Load load configure file
func (c *Configure) Load(basePath, filename string) (e error) {
	if filepath.IsAbs(filename) {
		filename = filepath.Clean(filename)
	} else {
		filename, e = filepath.Abs(filename)
		if e != nil {
			return
		}
	}
	var b []byte
	b, e = ioutil.ReadFile(filename)
	if e != nil {
		return
	}
	vm := jsonnet.MakeVM()
	vm.Importer(&jsonnet.FileImporter{})
	var jsonStr string
	jsonStr, e = vm.EvaluateSnippet(filename, string(b))
	if e != nil {
		return
	}
	b = []byte(jsonStr)
	e = json.Unmarshal(b, c)
	if e != nil {
		return
	}
	c.basePath = basePath
	c.filename = filename
	return
}
