package cmd

import (
	"{{.Package}}/cmd/daemon"
	"{{.Package}}/configure"
	"{{.Package}}/cookie"
	"{{.Package}}/logger"
	"{{.Package}}/management"
	"{{.Package}}/utils"
	"log"
	
	"github.com/spf13/cobra"
)

func init() {
	var (
		filename string
		debug    bool
		basePath = utils.BasePath()
	)
	cmd := &cobra.Command{
		Use:   "daemon",
		Short: "run as daemon",
		Run: func(cmd *cobra.Command, args []string) {
			// load configure
			cnf := configure.Single()
			e := cnf.Load(basePath, filename)
			if e != nil {
				log.Fatalln(e)
			}
			e = cnf.Format()
			if e != nil {
				log.Fatalln(e)
			}

			// init logger
			e = logger.Init(basePath, &cnf.Logger)
			if e != nil {
				log.Fatalln(e)
			}

			// init cookie
			e = cookie.Init(cnf.Cookie.Filename, cnf.Cookie.MaxAge)
			if e != nil {
				log.Fatalln(e)
			}

			// enable module
			management.Single().Enable(cnf.Module.Enable)

			// run
			daemon.Run(debug)
		},
	}
	flags := cmd.Flags()
	flags.StringVarP(&filename, "config",
		"c",
		utils.Abs(basePath, "{{.Project}}.jsonnet"),
		"configure file",
	)
	flags.BoolVarP(&debug, "debug",
		"d",
		false,
		"run as debug",
	)
	rootCmd.AddCommand(cmd)
}
