package daemon

import (
	"context"
	"crypto/tls"
	"net/http"
	"{{.Package}}/management"
	"strconv"

	"github.com/grpc-ecosystem/grpc-gateway/v2/runtime"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials"
	"google.golang.org/protobuf/proto"
)

func (s *Server) proxyServer(h2c bool, address string) (clientConn *grpc.ClientConn, mux *runtime.ServeMux, e error) {
	// 創建 客戶端
	var opts []grpc.DialOption
	if h2c {
		opts = append(opts, grpc.WithInsecure())
	} else {
		opts = append(opts, grpc.WithTransportCredentials(credentials.NewTLS(&tls.Config{
			InsecureSkipVerify: true,
		})))
	}
	clientConn, e = grpc.Dial(address, opts...)
	if e != nil {
		return
	}
	// 創建反向代理
	mux = runtime.NewServeMux(
		runtime.WithMarshalerOption(runtime.MIMEWildcard, &HTTPBodyMarshaler{
			Marshaler: &runtime.JSONPb{
				MarshalOptions: protojson.MarshalOptions{
					EmitUnpopulated: true,
				},
				UnmarshalOptions: protojson.UnmarshalOptions{
					DiscardUnknown: true,
				},
			},
		}),
		runtime.WithForwardResponseOption(httpResponseModifier),
		runtime.WithIncomingHeaderMatcher(headerToGRPC),
		runtime.WithOutgoingHeaderMatcher(grpcToHeader),		
	)
	// 註冊 反向代理
	mt := management.Single()
	e = mt.RegisterGateway(mux, clientConn)
	if e != nil {
		clientConn.Close()
		return
	}
	return
}
func headerToGRPC(key string) (string, bool) {
	switch key {
	case `Connection`:
		fallthrough
	case `Content-Type`:
		return key, false
	default:
		return key, true
	}
}
func grpcToHeader(key string) (string, bool) {
	switch key {
	case `x-http-code`:
		return key, false
	default:
		return key, true
	}
}
func httpResponseModifier(ctx context.Context, w http.ResponseWriter, p proto.Message) error {
	md, ok := runtime.ServerMetadataFromContext(ctx)
	if !ok {
		return nil
	}

	// set http status code
	if vals := md.HeaderMD.Get("x-http-code"); len(vals) > 0 {
		code, err := strconv.Atoi(vals[0])
		if err != nil {
			return err
		}
		w.WriteHeader(code)
		// delete the headers to not expose any grpc-metadata in http response
		delete(md.HeaderMD, "x-http-code")
	}

	return nil
}
