package daemon

import (
	"{{.Package}}/configure"
	"{{.Package}}/logger"
	"{{.Package}}/management"
	"net"
	"net/http"
	"os"
	"os/signal"
	"strings"
	"syscall"

	"github.com/grpc-ecosystem/grpc-gateway/v2/runtime"
	"go.uber.org/zap"
	"golang.org/x/net/http2"
	"golang.org/x/net/http2/h2c"
	"google.golang.org/grpc"
	"google.golang.org/grpc/reflection"
)

// Server 定義服務器
type Server struct {
	http2Server *http2.Server
	httpServer  *http.Server
	grpcServer  *grpc.Server
	clientConn  *grpc.ClientConn
	proxyMux    *runtime.ServeMux
	httpService *httpService
}

func runGRPC() {
	cnf := configure.Single().HTTP
	// 監聽端口
	l, e := net.Listen(`tcp`, cnf.Addr)
	if e != nil {
		if ce := logger.Logger.Check(zap.FatalLevel, "listen"); ce != nil {
			ce.Write(
				zap.Error(e),
			)
		}
		os.Exit(1)
	}
	defer l.Close()
	// 創建 服務器
	var server Server
	go func() {
		ch := make(chan os.Signal, 2)
		signal.Notify(ch,
			os.Interrupt,
			os.Kill,
			syscall.SIGTERM)
		for {
			sig := <-ch
			switch sig {
			case os.Interrupt:
				server.Stop()
				return
			case syscall.SIGTERM:
				server.Stop()
				return
			}
		}
	}()
	// 運行 服務器
	if cnf.H2() {
		if ce := logger.Logger.Check(zap.InfoLevel, "h2 work"); ce != nil {
			ce.Write(
				zap.String("addr", cnf.Addr),
			)
		}
		server.ServeTLS(l, cnf.CertFile, cnf.KeyFile)
	} else {
		if ce := logger.Logger.Check(zap.InfoLevel, "h2c work"); ce != nil {
			ce.Write(
				zap.String("addr", cnf.Addr),
			)
		}
		server.Serve(l)
	}
}

// Stop 關閉服務器
func (s *Server) Stop() {
	s.httpServer.Close()
}

// Serve 以 h2c 運行 服務
func (s *Server) Serve(l net.Listener) error {
	e := s.init(true, l.Addr().String(), "", "")
	if e != nil {
		return e
	}
	mt := management.Single()
	mt.OnStart()
	s.httpService = newHTTPService()
	s.httpServer.Handler = h2c.NewHandler(s, s.http2Server)
	e = s.httpServer.Serve(l)
	mt.OnStop()
	return e
}

// ServeTLS 以 h2 運行 服務
func (s *Server) ServeTLS(l net.Listener, certFile, keyFile string) error {
	e := s.init(false, l.Addr().String(), certFile, keyFile)
	if e != nil {
		return e
	}
	mt := management.Single()
	mt.OnStart()
	s.httpService = newHTTPService()
	s.httpServer.Handler = s
	e = s.httpServer.ServeTLS(l, certFile, keyFile)
	mt.OnStop()
	return e
}
func (s *Server) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	contextType := r.Header.Get(`Content-Type`)
	if strings.Contains(contextType, `application/grpc`) {
		s.grpcServer.ServeHTTP(w, r) // grpc 路由給 grpc服務
	} else {
		if s.httpService.CheckHTTP(r) {
			s.httpService.ServeHTTP(w, r) // 前端視圖 轉給 gin 處理
		} else {
			if r.Header.Get(`Authorization`) == `` {
				v := r.URL.Query().Get(`authorization`)
				if v != `` {
					r.Header.Set(`Authorization`, v)
				}
			}
			if r.Method == `GET` || r.Method == `HEAD` {
				r.Header.Set(`Method`, r.Method)
			}
			s.proxyMux.ServeHTTP(w, r) // 非 grpc 路由給 http 代理服務器
		}
	}
}
func (s *Server) init(h2c bool, address, certFile, keyFile string) (e error) {
	var httpServer http.Server
	var http2Server http2.Server
	// 創建 反向代理
	clientConn, mux, e := s.proxyServer(h2c, address)
	if e != nil {
		return
	}
	// 配置 http2
	e = http2.ConfigureServer(&httpServer, &http2Server)
	if e != nil {
		clientConn.Close()
		return
	}
	//創建 rpc 服務器
	mt := management.Single()
	grpcServer := grpc.NewServer(
		grpc.UnaryInterceptor(mt.UnaryInterceptor),
		grpc.StreamInterceptor(mt.StreamInterceptor),
	)
	//註冊 服務

	mt.RegisterGRPC(grpcServer)
	//註冊 反射 到 服務 路由
	reflection.Register(grpcServer)

	s.httpServer = &httpServer
	s.http2Server = &http2Server
	s.grpcServer = grpcServer
	s.clientConn = clientConn
	s.proxyMux = mux
	return
}
