package daemon

import (
	"net/http"

	"{{.Package}}/web/api"
	// "{{.Package}}/web/static"
	// "{{.Package}}/web/view"
	"strings"

	"{{.Package}}/web"

	"github.com/gin-gonic/gin"
)

type httpService struct {
	router *gin.Engine
}

func newHTTPService() *httpService {
	router := gin.Default()
	rs := []web.IHelper{
		api.Helper{},
		// static.Helper{},
		// view.Helper{},
	}
	for _, r := range rs {
		r.Register(&router.RouterGroup)
	}
	return &httpService{
		router: router,
	}
}

// CheckHTTP .
func (s *httpService) CheckHTTP(r *http.Request) bool {
	header := r.Header
	if r.Method == "GET" &&
		strings.Contains(strings.ToLower(header.Get("Connection")), "upgrade") &&
		strings.EqualFold(header.Get("Upgrade"), "websocket") {
		return true
	}
	path := r.URL.Path
	if strings.HasPrefix(path, `/api/v1/features`) ||
		strings.HasPrefix(path, `/api/v1/system`) ||
		strings.HasPrefix(path, `/api/v1/session`) {
		return false
	}
	return true
}
func (s *httpService) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	s.router.ServeHTTP(w, r)
}
