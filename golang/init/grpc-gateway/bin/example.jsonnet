local Millisecond = 1;
local Second = 1000 * Millisecond;
local Minute = 60 * Second;
local Hour = 60 * Minute;
local Day = 24 * Hour;
{
	HTTP: {
		Addr: "127.0.0.1:6000",
		// x509 if empty use h2c
		// CertFile: "test.pem",
		// KeyFile: "test.key",
		// http body limit default 32k
		MaxBytesReader: 1024*32,
	},
	Module: {
		// enable module
		Enable: [
			"session",
			"system.debug",
			"features.logger",
		],
		// module configure
		Data: {
			"session": [
				{
					Name: "king",
					Password: "12345678",
					Authorization: [
						960316,
					],
				},
				{
					Name: "kate",
					Password: "12345678",
					Authorization: [
					],
				},
			],
		},
	},
	Cookie: {
		// Filename:"securecookie.json"
		// MaxAge:Day,
	},
	Logger: {
		// log name
		//Filename:"logs/{{.Project}}.log",
		// MB
		MaxSize:    100, 
		// number of files
		MaxBackups: 3,
		// day
		MaxAge:     28,
		// 是否要 輸出 代碼位置
		Caller:true,
		// 檔案日誌等級 debug info warn error dpanic panic fatal
		FileLevel: "debug",
		// 控制檯日誌等級 debug info warn error dpanic panic fatal
		ConsoleLevel: "debug",
	},
}