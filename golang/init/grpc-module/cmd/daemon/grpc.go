package daemon

import (
	"{{.Package}}/configure"
	"{{.Package}}/logger"
	"{{.Package}}/management"
	"net"
	"os"
	"os/signal"
	"syscall"

	"go.uber.org/zap"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials"
	"google.golang.org/grpc/reflection"
)

func runGRPC() {
	cnf := configure.Single().GRPC

	l, e := net.Listen("tcp", cnf.Addr)
	if e != nil {
		if ce := logger.Logger.Check(zap.FatalLevel, "listen"); ce != nil {
			ce.Write(
				zap.Error(e),
			)
		}
		os.Exit(1)
	}

	mt := management.Single()
	var srv *grpc.Server
	opt := []grpc.ServerOption{
		grpc.UnaryInterceptor(mt.UnaryInterceptor),
		grpc.StreamInterceptor(mt.StreamInterceptor),
	}
	if cnf.H2() {
		creds, e := credentials.NewServerTLSFromFile(cnf.CertFile, cnf.KeyFile)
		if e != nil {
			if ce := logger.Logger.Check(zap.FatalLevel, "NewServerTLSFromFile"); ce != nil {
				ce.Write(
					zap.Error(e),
				)
			}
			os.Exit(1)
		}
		opt = append(opt, grpc.Creds(creds))
		srv = grpc.NewServer(
			opt...,
		)
		if ce := logger.Logger.Check(zap.InfoLevel, "h2 work"); ce != nil {
			ce.Write(
				zap.String("addr", cnf.Addr),
			)
		}
	} else {
		srv = grpc.NewServer(
			opt...,
		)
		if ce := logger.Logger.Check(zap.InfoLevel, "h2c work"); ce != nil {
			ce.Write(
				zap.String("addr", cnf.Addr),
			)
		}
	}
	mt.RegisterGRPC(srv)
	reflection.Register(srv)

	go func() {
		ch := make(chan os.Signal, 2)
		signal.Notify(ch,
			os.Interrupt,
			os.Kill,
			syscall.SIGTERM)
		for {
			sig := <-ch
			switch sig {
			case os.Interrupt:
				srv.Stop()
				return
			case syscall.SIGTERM:
				srv.Stop()
				return
			}
		}
	}()
	mt.OnStart()
	e = srv.Serve(l)
	mt.OnStop()
	if e != nil {
		if ce := logger.Logger.Check(zap.FatalLevel, "grpc Serve"); ce != nil {
			ce.Write(
				zap.Error(e),
			)
		}
		os.Exit(1)
		return
	}
}
