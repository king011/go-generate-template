package management

import (
	"encoding/json"

	"google.golang.org/grpc"
)

// Module 定義了子模塊 接口
type Module interface {
	// ID 返回 唯一的子模塊 名稱 對於字母必須使用 小寫
	ID() string

	// OnStart 初始化 回調 應該在此 執行 模塊 初始化
	OnStart(basePath string, data json.RawMessage)

	// RegisterGRPC 爲子模塊 註冊 grpc 服務
	RegisterGRPC(srv *grpc.Server, middleware *Middleware)

	// OnReload 模塊重載配置 回調
	OnReload(basePath string, data json.RawMessage, tag string) (e error)

	// 通知 子模塊 清空 數據庫 緩存
	//
	// tag 是一個 自定義標記 通常是 表名 子模塊 依據此值來確定 要清除那些緩存
	//
	// 通常 tag 如果爲 空字符串 則應該 清除 全部 緩存
	OnClearDBCache(tag string) error

	// 通知 子模塊 清空 緩存
	//
	// tag 是一個 自定義標記 通常是 子模塊 依據此值來確定 要清除那些緩存
	//
	// 通常 tag 如果爲 空字符串 則應該 清除 全部 緩存
	OnClearCache(tag string) error

	// OnStop 停止 回調 應該在此 執行 資源釋放 和 停止模塊工作
	OnStop()
}
