package cookie

import (
	"context"
	"encoding/gob"
	"{{.Package}}/logger"
	"{{.Package}}/management"

	"go.uber.org/zap"
	"google.golang.org/grpc/metadata"
)

func init() {
	gob.Register(&Session{})
}

// Session user session info
type Session struct {
	Name          string
	Authorization []int64
}

// Who .
func (s *Session) Who() string {
	return s.Name
}

// Cookie encode to cookie
func (s *Session) Cookie() (string, error) {
	return Encode("session", s)
}

// IsRoot if user is root return true
func (s *Session) IsRoot() (yes bool) {
	count := len(s.Authorization)
	for i := 0; i < count; i++ {
		if s.Authorization[i] == 960316 {
			return true
		}
	}
	return
}

// FromContext restore session from context
func FromContext(ctx context.Context) (session *Session, e error) {
	keys, ok := management.FromContext(ctx)
	if ok {
		v := keys.Get(`session`)
		if v != nil {
			if session, ok = v.(*Session); ok {
				return
			} else if !ok {
				if ce := logger.Logger.Check(zap.ErrorLevel, `session FromContext`); ce != nil {
					ce.Write(
						zap.String(`error`, `md type error`),
					)
				}
			}
		}
	}

	md, ok := metadata.FromIncomingContext(ctx)
	if ok {
		session, e = FromMD(md)
	}
	if session != nil && keys != nil {
		keys.Set(`session`, session)
	}
	return
}

// FromMD restore session from MD
func FromMD(md metadata.MD) (session *Session, e error) {
	strs := md.Get(`Authorization`)
	if len(strs) > 0 {
		session, e = FromCookie(strs[0])
		return
	}
	return
}

// FromCookie restore session from cookie
func FromCookie(val string) (session *Session, e error) {
	var s Session
	e = Decode("session", val, &s)
	if e != nil {
		return
	}
	session = &s
	return
}
