package logger

import (
	"encoding/json"

	"{{.Package}}/logger"
	"{{.Package}}/management"

	grpc_logger "{{.Package}}/protocol/features/logger"

	"go.uber.org/zap"

	"google.golang.org/grpc"
)

// ModuleID 子模塊 名稱
const ModuleID = "features.logger"

func init() {
	// 註冊 模塊
	management.Single().Register(&_Module{})
}

// _Module 實現模塊接口
type _Module struct {
}

// ID 返回 唯一的子模塊 名稱
func (m *_Module) ID() string {
	return ModuleID
}

// RegisterGRPC 爲模塊 註冊 grpc 服務
func (m *_Module) RegisterGRPC(srv *grpc.Server, middleware *management.Middleware) {
	// 註冊 grpc
	var impl _Impl
	grpc_logger.RegisterServiceServer(srv, impl)

	// 註冊模塊 中間件
	helper := middleware.Module(ModuleID)

	helper.
		ModuleUnary(
			impl.UnaryInterceptorCheckRoot,
		).
		ModuleStream(
			impl.StreamInterceptorCheckRoot,
		)

}

// OnStart .
func (_Module) OnStart(basePath string, data json.RawMessage) {
	logger.Logger.Info("OnStart",
		zap.String("module", ModuleID),
	)
}

// OnReload .
func (_Module) OnReload(basePath string, data json.RawMessage, tag string) (e error) {
	logger.Logger.Info("OnReload",
		zap.String("module", ModuleID),
	)
	return
}

// OnStop .
func (_Module) OnStop() {
	logger.Logger.Info("OnStop",
		zap.String("module", ModuleID),
	)
}

// OnClearDBCache .
func (_Module) OnClearDBCache(tag string) error {
	logger.Logger.Info("OnClearDBCache",
		zap.String("module", ModuleID),
		zap.String("tag", tag),
	)
	return nil
}

// OnClearCache .
func (_Module) OnClearCache(tag string) error {
	logger.Logger.Info("OnClearCache",
		zap.String("module", ModuleID),
		zap.String("tag", tag),
	)
	return nil
}
