package module

import (
	"context"

	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

// UnaryInterceptorCheckSession 一元攔截器 驗證用戶 登入
func (s Service) UnaryInterceptorCheckSession(ctx context.Context, req interface{}, info *grpc.UnaryServerInfo, handler grpc.UnaryHandler) (response interface{}, e error) {
	_, e = s.Session(ctx)
	if e != nil {
		return
	}
	response, e = handler(ctx, req)
	return
}

// StreamInterceptorCheckSession 流式攔截器 驗證用戶 登入
func (s Service) StreamInterceptorCheckSession(srv interface{}, ss grpc.ServerStream, info *grpc.StreamServerInfo, handler grpc.StreamHandler) (e error) {
	ctx := ss.Context()
	_, e = s.Session(ctx)
	if e != nil {
		return
	}
	e = handler(srv, ss)
	return
}

// UnaryInterceptorCheckRoot 一元攔截器 驗證用戶 超級管理員
func (s Service) UnaryInterceptorCheckRoot(ctx context.Context, req interface{}, info *grpc.UnaryServerInfo, handler grpc.UnaryHandler) (response interface{}, e error) {
	session, e := s.Session(ctx)
	if e != nil {
		return
	} else if !session.IsRoot() {
		e = status.Error(codes.PermissionDenied, `only root can access`)
		return
	}
	response, e = handler(ctx, req)
	return
}

// StreamInterceptorCheckRoot 流式攔截器 驗證用戶 超級管理員
func (s Service) StreamInterceptorCheckRoot(srv interface{}, ss grpc.ServerStream, info *grpc.StreamServerInfo, handler grpc.StreamHandler) (e error) {
	ctx := ss.Context()
	session, e := s.Session(ctx)
	if e != nil {
		return
	} else if !session.IsRoot() {
		e = status.Error(codes.PermissionDenied, `only root can access`)
		return
	}
	e = handler(srv, ss)
	return
}
