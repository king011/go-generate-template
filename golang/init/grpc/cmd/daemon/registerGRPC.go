package daemon

import (
	"context"
	"{{.Package}}/logger"
	grpc_logger "{{.Package}}/protocol/logger"

	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"
	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

func registerGRPC(s *grpc.Server) {
	grpc_logger.RegisterServiceServer(s, _Logger{})
}

type _Logger struct {
}
func (_Logger) List(ctx context.Context, request *grpc_logger.ListRequest) (response *grpc_logger.ListResponse, e error) {
	TAG := "logger List"

	file, e := logger.Logger.FileLevel().MarshalText()
	if e != nil {
		if ce := logger.Logger.Check(zap.ErrorLevel, TAG); ce != nil {
			ce.Write()
		}
		return
	}
	console, e := logger.Logger.ConsoleLevel().MarshalText()
	if e != nil {
		if ce := logger.Logger.Check(zap.ErrorLevel, TAG); ce != nil {
			ce.Write()
		}
		return
	}

	response = &grpc_logger.ListResponse{
		File:    string(file),
		Console: string(console),
	}
	if ce := logger.Logger.Check(zap.DebugLevel, TAG); ce != nil {
		ce.Write()
	}
	return
}
func (_Logger) Patch(ctx context.Context, request *grpc_logger.PatchRequest) (response *grpc_logger.PatchResponse, e error) {
	TAG := "logger Patch"
	var (
		at    zap.AtomicLevel
		level zapcore.Level
	)
	switch request.Level {
	case "debug":
		level = zap.DebugLevel
	case "info":
		level = zap.InfoLevel
	case "warn":
		level = zap.WarnLevel
	case "error":
		level = zap.ErrorLevel
	case "dpanic":
		level = zap.DPanicLevel
	case "panic":
		level = zap.PanicLevel
	case "fatal":
		level = zap.FatalLevel
	default:
		e = status.Error(codes.InvalidArgument, `not support level`)
	}
	if e != nil {
		if ce := logger.Logger.Check(zap.WarnLevel, TAG); ce != nil {
			ce.Write(
				zap.String(`tag`, request.Tag),
				zap.String(`level`, request.Level),
			)
		}
		return
	}

	switch request.Tag {
	case "file":
		at = logger.Logger.FileLevel()
	case "console":
		at = logger.Logger.ConsoleLevel()
	default:
		e = status.Error(codes.InvalidArgument, `not support tag`)
	}
	if e != nil {
		if ce := logger.Logger.Check(zap.WarnLevel, TAG); ce != nil {
			ce.Write(
				zap.String(`tag`, request.Tag),
				zap.String(`level`, request.Level),
			)
		}
		return
	}
	at.SetLevel(level)

	response = &grpc_logger.PatchResponse{}
	if ce := logger.Logger.Check(zap.InfoLevel, TAG); ce != nil {
		ce.Write(
			zap.String(`tag`, request.Tag),
			zap.String(`level`, request.Level),
		)
	}
	return
}
func (_Impl) Atach(request *grpc_logger.AttachRequest, stream grpc_logger.Service_AtachServer) (e error) {
	// TAG := "features.logger Atach"
	ctx := stream.Context()

	done := ctx.Done()
	listener := logger.NewSnapshotListener(done)
	logger.AddListener(listener)
	var (
		working = true
		ch      = listener.Channel()
		data    []byte
		respose grpc_logger.AttachResponse
	)
	for working {
		select {
		case <-done:
			working = false
			e = ctx.Err()
		case data = <-ch:
			if len(data) > 0 {
				respose.Data = data
				e = stream.Send(&respose)
				if e != nil {
					working = false
				}
			}
		}
	}
	logger.RemoveListener(listener)
	return
}