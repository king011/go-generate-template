package configure

import (
	"math"
	"time"
)

// Gate 網關配置
type Gate struct {
	// 同時在線上限
	MaxConnNum int
	// 每個連接 write 隊列大小
	PendingWriteNum int
	// 每個消息最大長度
	MaxMsgLen uint32
	// websocket 監聽地址
	WSAddr string
	// websocket 握手超時時間
	HTTPTimeout time.Duration
	// http 證書
	CertFile string
	KeyFile  string
	// tcp 監聽地址
	TCPAddr string
	// 消息包 len 長度
	LenMsgLen int
	// 消息包 len 是否使用小端序
	LittleEndian bool
}

// Format format global configure
func (g *Gate) Format(basePath string) (e error) {
	if g.MaxConnNum < 1 {
		g.MaxConnNum = 20000
	}
	if g.PendingWriteNum < 1 {
		g.PendingWriteNum = 100
	}
	if g.MaxMsgLen < 1 {
		g.MaxMsgLen = math.MaxUint16
	}
	if g.HTTPTimeout > 1 {
		g.HTTPTimeout *= time.Millisecond
	} else {
		g.HTTPTimeout = time.Second * 10
	}
	if g.LenMsgLen < 1 {
		g.LenMsgLen = 2
	}
	return
}
