package internal

import (
	"github.com/name5566/leaf/chanrpc"
	"github.com/name5566/leaf/module"
)

// NewSkeleton .
func NewSkeleton() *module.Skeleton {
	skeleton := &module.Skeleton{
		GoLen:              0,
		TimerDispatcherLen: 0,
		AsynCallLen:        0,
		ChanRPCServer:      chanrpc.NewServer(0),
	}
	skeleton.Init()
	return skeleton
}
