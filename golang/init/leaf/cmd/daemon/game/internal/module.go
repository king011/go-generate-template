package internal

import (
	"github.com/name5566/leaf/module"
)

var (
	skeleton = NewSkeleton()
	// ChanRPC .
	ChanRPC = skeleton.ChanRPCServer
)

// Module .
type Module struct {
	*module.Skeleton
}

// OnInit .
func (m *Module) OnInit() {
	m.Skeleton = skeleton
}

// OnDestroy .
func (m *Module) OnDestroy() {

}
