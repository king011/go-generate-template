package game

import (
	"{{.Package}}/cmd/daemon/game/internal"
	"{{.Package}}/cmd/daemon/game/internal/processor/json"

	"github.com/name5566/leaf/chanrpc"
	"github.com/name5566/leaf/module"
)

var (
	// Module . 
	Module  = new(internal.Module)
	// ChanRPC . 
	ChanRPC = internal.ChanRPC
	// Processor .
	Processor = ProcessorHelper{json.NewProcessor()}

	keys = map[string]*chanrpc.Server{
		"game": internal.ChanRPC,
	}
)

// NewSkeleton .
func NewSkeleton() *module.Skeleton {
	return internal.NewSkeleton()
}

// SetChanRPC .
func SetChanRPC(id string, s *chanrpc.Server) {
	if _, ok := keys[id]; ok {
		panic(`chanrpc already exists : ` + id)
	}
	keys[id] = s
}

// GetChanRPC .
func GetChanRPC(id string) *chanrpc.Server {
	s, ok := keys[id]
	if !ok {
		panic(`chanrpc not exists : ` + id)
	}
	return s
}

// ProcessorHelper .
type ProcessorHelper struct {
	*json.Processor
}

// InitHandler Register msg,then SetRouter, then
func (p *ProcessorHelper) InitHandler(msg interface{}, msgRouter *chanrpc.Server, msgHandler json.MsgHandler) {
	p.Register(msg)
	p.SetRouter(msg, msgRouter)
	p.SetHandler(msg, msgHandler)
}