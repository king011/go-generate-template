package login

import (
	"{{.Package}}/cmd/daemon/login/internal"
)

var (
	// Module . 
	Module = new(internal.Module)
)
