package internal

import (
	"fmt"
	"{{.Package}}/cmd/daemon/game"

	"github.com/name5566/leaf/gate"
	"github.com/name5566/leaf/module"
)

const (
	// ModuleID .
	ModuleID = "login"
)

var (
	skeleton = game.NewSkeleton()
)

func init() {
	// 註冊 ChanRPCServer
	game.SetChanRPC(ModuleID, skeleton.ChanRPCServer)
}

// A Module impl leaf module
type Module struct {
	*module.Skeleton
}

// OnInit .
func (m *Module) OnInit() {
	m.Skeleton = skeleton

	// 註冊處理函數
	game.Processor.Register(&responseLogin{})
	game.Processor.InitHandler(&requestLogin{}, skeleton.ChanRPCServer, m.login)
}

// OnDestroy .
func (m *Module) OnDestroy() {
}

func (m *Module) login(args []interface{}) {
	request := args[0].(*requestLogin)
	fmt.Println(`login`, request.Name, request.Password)
	a := args[1].(gate.Agent)
	a.SetUserData(request.Name)
	if request.Name == `king` && request.Password == `123` {
		a.WriteMsg(&responseLogin{
			OK: true,
		})
	} else {
		a.WriteMsg(&responseLogin{
			Error: `name or password not match`,
		})
	}

}

type responseLogin struct {
	OK    bool   `json:"ok,omitempty"`
	Error string `json:"error,omitempty"`
}

// MessageID .
func (responseLogin) MessageID() string {
	return `ResponseLogin at ` + ModuleID
}

type requestLogin struct {
	Name     string `json:"name,omitempty"`
	Password string `json:"password,omitempty"`
}

// MessageID .
func (requestLogin) MessageID() string {
	return `RequestLogin at ` + ModuleID
}
