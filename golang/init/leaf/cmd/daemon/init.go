package daemon

import (
	"{{.Package}}/cmd/daemon/game"
	"{{.Package}}/cmd/daemon/gate"
	"{{.Package}}/cmd/daemon/login"

	"github.com/name5566/leaf"
)

// Run run as deamon
func Run() {
	leaf.Run(
		game.Module, gate.Module, login.Module,
	)
}
