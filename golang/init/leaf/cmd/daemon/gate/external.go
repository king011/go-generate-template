package gate

import (
	"{{.Package}}/cmd/daemon/gate/internal"
)

var (
	// Module . 
	Module = new(internal.Module)
)
