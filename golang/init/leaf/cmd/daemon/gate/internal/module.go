package internal

import (
	"{{.Package}}/cmd/daemon/game"
	"{{.Package}}/configure"

	"github.com/name5566/leaf/gate"
)

// A Module impl leaf module
type Module struct {
	*gate.Gate
}

// OnInit .
func (m *Module) OnInit() {
	cnf := configure.Single().Gate
	m.Gate = &gate.Gate{
		MaxConnNum:      cnf.MaxConnNum,
		PendingWriteNum: cnf.PendingWriteNum,
		MaxMsgLen:       cnf.MaxMsgLen,
		WSAddr:          cnf.WSAddr,
		HTTPTimeout:     cnf.HTTPTimeout,
		CertFile:        cnf.CertFile,
		KeyFile:         cnf.KeyFile,
		TCPAddr:         cnf.TCPAddr,
		LenMsgLen:       cnf.LenMsgLen,
		LittleEndian:    cnf.LittleEndian,
		Processor:       game.Processor,
		AgentChanRPC:    game.ChanRPC,
	}
}

// OnDestroy .
func (m *Module) OnDestroy() {

}
