local Millisecond = 1;
local Second = 1000 * Millisecond;
local Minute = 60 * Second;
local Hour = 60 * Minute;
local Day = 24 * Hour;
{
	// 網關配置
	Gate: {
		// 同時在線上限
		MaxConnNum: 20000,
		// 每個連接 write 隊列大小
		PendingWriteNum: 100,
		// 每個消息最大長度
		MaxMsgLen: 65535,
		// websocket 監聽地址
		WSAddr: "",
		// websocket 握手超時時間
		HTTPTimeout: Second * 10,
		// http 證書
		CertFile: "",
		KeyFile: "",
		// tcp 監聽地址
		TCPAddr: ":9000",
		// 消息包 len 長度
		LenMsgLen: 2,
		// 消息包 len 是否使用小端序
		LittleEndian: false,
	},
	Logger:{
		// zap http
		//HTTP:"localhost:20000",
		// log name
		//Filename:"logs/{{.Project}}.log",
		// MB
		MaxSize:    100, 
		// number of files
		MaxBackups: 3,
		// day
		MaxAge:     28,
		// level : debug info warn error dpanic panic fatal
		Level :"debug",
		// 是否要 輸出 代碼位置
		Caller:true,
	},
}