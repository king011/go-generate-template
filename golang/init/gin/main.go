package main

import (
	// _ "{{.Package}}/assets/en-US/statik"
	// _ "{{.Package}}/assets/static/statik"
	// _ "{{.Package}}/assets/zh-Hans/statik"
	// _ "{{.Package}}/assets/zh-Hant/statik"
	"{{.Package}}/cmd"
	"log"	
)

func main() {
	log.SetFlags(log.LstdFlags | log.Lshortfile)
	if e := cmd.Execute(); e != nil {
		log.Fatalln(e)
	}
}
