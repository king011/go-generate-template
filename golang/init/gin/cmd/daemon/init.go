package daemon

import (
	"net"
	"net/http"
	"os"

	"github.com/gin-gonic/gin"
	"golang.org/x/net/http2"
	"golang.org/x/net/http2/h2c"

	"go.uber.org/zap"

	"{{.Package}}/configure"
	"{{.Package}}/logger"
)

func getMessage(tls, http2 bool) (message string) {
	if tls {
		if http2 {
			message = "h2 work"
		} else {
			message = `https work`
		}
	} else {
		if http2 {
			message = "h2c work"
		} else {
			message = `http work`
		}
	}
	return
}

// Run run as deamon
func Run(release bool) {
	if release {
		gin.SetMode(gin.ReleaseMode)
	}
	// listen tcp
	cnf := configure.Single().HTTP
	l, e := net.Listen(`tcp`, cnf.Addr)
	if e != nil {
		if ce := logger.Logger.Check(zap.FatalLevel, `listen error`); ce != nil {
			ce.Write(
				zap.Error(e),
			)
		}
		os.Exit(1)
	}

	// log
	tls := cnf.TLS()
	message := getMessage(tls, cnf.HTTP2)
	if ce := logger.Logger.Check(zap.InfoLevel, message); ce != nil {
		ce.Write(
			zap.String(`addr`, cnf.Addr),
		)
	}

	// mux
	router := newGIN()

	// http
	var httpServer http.Server
	if cnf.HTTP2 {
		var http2Server http2.Server
		e = http2.ConfigureServer(&httpServer, &http2Server)
		if e != nil {
			if ce := logger.Logger.Check(zap.FatalLevel, `configure http2 error`); ce != nil {
				ce.Write(
					zap.Error(e),
				)
			}
			os.Exit(1)
			return
		}
		if tls {
			httpServer.Handler = router
		} else {
			httpServer.Handler = h2c.NewHandler(router, &http2Server)
		}
	} else {
		httpServer.Handler = router
	}

	// serve
	if tls {
		e = httpServer.ServeTLS(l, cnf.CertFile, cnf.KeyFile)
	} else {
		e = httpServer.Serve(l)
	}
	if ce := logger.Logger.Check(zap.FatalLevel, `serve error`); ce != nil {
		ce.Write(
			zap.Error(e),
		)
	}
	os.Exit(1)
}