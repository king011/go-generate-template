package daemon

import (
	"{{.Package}}/web"
	"{{.Package}}/web/api"
	// "{{.Package}}/web/static"
	// "{{.Package}}/web/view"

	"github.com/gin-gonic/gin"
)

func newGIN() (router *gin.Engine) {
	router = gin.Default()
	rs := []web.IHelper{
		api.Helper{},
		// static.Helper{},
		// view.Helper{},
	}
	for _, r := range rs {
		r.Register(&router.RouterGroup)
	}
	return
}
