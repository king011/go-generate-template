package cmd

import (
	"{{.Package}}/cmd/daemon"
	"{{.Package}}/configure"
	"{{.Package}}/cookie"
	"{{.Package}}/logger"
	"{{.Package}}/utils"
	"log"

	"github.com/spf13/cobra"
)

func init() {
	var filename string
	var release bool
	basePath := utils.BasePath()
	cmd := &cobra.Command{
		Use:   "daemon",
		Short: "run as daemon",
		Run: func(cmd *cobra.Command, args []string) {
			// load configure
			cnf := configure.Single()
			e := cnf.Load(filename)
			if e != nil {
				log.Fatalln(e)
			}
			e = cnf.Format(basePath)
			if e != nil {
				log.Fatalln(e)
			}

			// init logger
			e = logger.Init(basePath, &cnf.Logger)
			if e != nil {
				log.Fatalln(e)
			}
			
			// init cookie
			e = cookie.Init(cnf.Cookie.Filename, cnf.Cookie.MaxAge)
			if e != nil {
				log.Fatalln(e)
			}

			// run
			daemon.Run(release)
		},
	}
	flags := cmd.Flags()
	flags.StringVarP(&filename, "config",
		"c",
		utils.Abs(basePath, "{{.Project}}.jsonnet"),
		"configure file",
	)
	flags.BoolVarP(&release, "release",
		"r",
		false,
		"run as release",
	)
	rootCmd.AddCommand(cmd)
}
