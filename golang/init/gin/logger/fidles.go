package logger

import (
	"go.uber.org/zap"
)

// IZapFields 接口
type IZapFields interface {
	ZapFields() []zap.Field
}

// Fields 創建 zap.Field 切片 實現 IZapFields 接口
func Fields(fields ...zap.Field) ZapFields {
	return fields
}

// ZapFields 爲 []zap.Field 實現 IZapFields 接口
type ZapFields []zap.Field

// ZapFields 實現 IZapFields 接口
func (arrs ZapFields) ZapFields() []zap.Field {
	return arrs
}

// JoinFields Join Fields
func JoinFields(arrs ...IZapFields) []zap.Field {
	count := len(arrs)
	if count == 0 {
		return nil
	}
	items := make([][]zap.Field, count)
	sum := 0
	for i := 0; i < count; i++ {
		items[i] = arrs[i].ZapFields()
		sum += len(items[i])
	}
	switch count {
	case 1:
		return items[0]
	case 2:
		return append(items[0], items[1]...)
	}
	dst := make([]zap.Field, sum)
	pos := 0
	for _, item := range items {
		pos += copy(dst[pos:], item)
	}
	return dst
}
