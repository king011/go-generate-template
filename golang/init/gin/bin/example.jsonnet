local Millisecond = 1;
local Second = 1000 * Millisecond;
local Minute = 60 * Second;
local Hour = 60 * Minute;
local Day = 24 * Hour;

local KB=1024;
local MB=KB * 1024;
local GB=MB * 1024;

{
	HTTP: {
		Addr:":9000",
		// x509 if empty use h2c
		// CertFile:"test.pem",
		// KeyFile:"test.key",

		// 是否 使用 http2
		HTTP2: false,

		// 設定 http 請求 body 最大尺寸
		// 如果 == 0 使用默認值 32 KB
		// 如果 < 0 不限制
		MaxBytesReader: 5 * MB,
	},
	Logger: {
		// log name
		//Filename:"logs/{{.Project}}.log",
		// MB
		MaxSize:    100, 
		// number of files
		MaxBackups: 3,
		// day
		MaxAge:     28,
		// 是否要 輸出 代碼位置
		Caller:true,
		// 檔案日誌等級 debug info warn error dpanic panic fatal
		FileLevel: "debug",
		// 控制檯日誌等級 debug info warn error dpanic panic fatal
		ConsoleLevel: "debug",
	},
}