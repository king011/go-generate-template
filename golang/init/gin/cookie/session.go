package cookie

import (
	"encoding/gob"
)

func init() {
	gob.Register(&Session{})
}

// Session user session info
type Session struct {
	Name string
	Root bool
}

// Cookie encode to cookie
func (s *Session) Cookie() (string, error) {
	return Encode("session", s)
}

// IsRoot if user is root return true
func (s *Session) IsRoot() (yes bool) {
	yes = s.Root
	return
}

// FromCookie restore session from value
func FromCookie(val string) (session *Session, e error) {
	var s Session
	e = Decode("session", val, &s)
	if e != nil {
		return
	}
	session = &s
	return
}
