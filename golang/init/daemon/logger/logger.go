package logger

import (
	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"
	"gopkg.in/natefinch/lumberjack.v2"
)

const bufferSize = 1024 * 32

// Options 日誌 選項
type Options struct {
	// 日誌 檔案名 如果爲空 則 不記錄到檔案
	Filename string
	// 單個日誌檔案 大小上限 MB
	MaxSize int
	// 保存 多少個 日誌 檔案
	MaxBackups int
	// 保存 多少天內的 日誌
	MaxDays int
	// 是否要 輸出 代碼位置
	Caller bool
	// 檔案日誌等級 debug info warn error dpanic panic fatal
	FileLevel string
	// 控制檯日誌等級 debug info warn error dpanic panic fatal
	ConsoleLevel string
}

type noCopy struct{}

func (*noCopy) Lock()   {}
func (*noCopy) Unlock() {}

// Helper zap 日誌
type Helper struct {
	noCopy noCopy
	// zap 記錄器
	*zap.Logger

	fileLevel    zap.AtomicLevel
	consoleLevel zap.AtomicLevel
}

var emptyAtomicLevel = zap.NewAtomicLevel()

// Attach 附加到 已存在的 日誌記錄器
func (l *Helper) Attach(src *Helper) {
	l.Logger = src.Logger
	l.fileLevel = src.fileLevel
	l.consoleLevel = src.consoleLevel
}

// Detach 分離 Logger
func (l *Helper) Detach() {
	l.Logger = nil
	l.fileLevel = emptyAtomicLevel
	l.consoleLevel = emptyAtomicLevel
}

// FileLevel 返回 檔案日誌等級
func (l *Helper) FileLevel() zap.AtomicLevel {
	return l.fileLevel
}

// ConsoleLevel 返回 控制檯
func (l *Helper) ConsoleLevel() zap.AtomicLevel {
	return l.consoleLevel
}

// NewHelper 創建 一個 日誌記錄器
//
// 如果 options 爲空 則 創建一個 默認的 記錄器
func NewHelper(options *Options, zapOptions ...zap.Option) *Helper {
	var cores []zapcore.Core
	fileLevel := zap.NewAtomicLevel()
	consoleLevel := zap.NewAtomicLevel()
	// 檔案日誌
	fileLevel = zap.NewAtomicLevel()
	if options.FileLevel == "" {
		fileLevel.SetLevel(zap.FatalLevel)
	} else if e := fileLevel.UnmarshalText([]byte(options.FileLevel)); e != nil {
		fileLevel.SetLevel(zap.FatalLevel)
	}
	cores = append(cores, zapcore.NewCore(
		zapcore.NewJSONEncoder(zap.NewProductionEncoderConfig()),
		newLoggerCache(zapcore.AddSync(&lumberjack.Logger{
			Filename:   options.Filename,
			MaxSize:    options.MaxSize, // megabytes
			MaxBackups: options.MaxBackups,
			MaxAge:     options.MaxDays, // days
		}), bufferSize),
		fileLevel,
	))

	// 控制檯
	if options.ConsoleLevel == "" {
		consoleLevel.SetLevel(zap.FatalLevel)
	} else if e := consoleLevel.UnmarshalText([]byte(options.ConsoleLevel)); e != nil {
		consoleLevel.SetLevel(zap.FatalLevel)
	}
	cores = append(cores, zapcore.NewCore(
		zapcore.NewConsoleEncoder(zap.NewDevelopmentEncoderConfig()),
		newLoggerCache(monitor, bufferSize),
		consoleLevel,
	))

	if options.Caller {
		zapOptions = append(zapOptions, zap.AddCaller())
	}

	return &Helper{
		Logger: zap.New(
			zapcore.NewTee(cores...),
			zapOptions...,
		),
		fileLevel:    fileLevel,
		consoleLevel: consoleLevel,
	}
}
