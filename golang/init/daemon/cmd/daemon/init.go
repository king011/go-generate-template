package daemon

import (
	"{{.Package}}/configure"
	"{{.Package}}/logger"

	"go.uber.org/zap"
)

// Run run as deamon
func Run() {
	cnf := configure.Single()
	logger.Logger.Info("daemon running",
		zap.String("console", cnf.Logger.ConsoleLevel),
		zap.String("file", cnf.Logger.FileLevel),
	)
}
